package com.gft.discoveryClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
//@EnableCircuitBreaker
@RestController
public class DiscoveryClientApplication {

	@Autowired
	private DiscoveryImpl discoveryClientApplication;

	@Autowired
	private FeignDiscovery discovery;

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryClientApplication.class, args);
	}

	@LoadBalanced
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@RequestMapping(value = "/discovery/{name}", produces = "application/json")
	public Object getCarWithClientDiscovery(@PathVariable String name) {
		return discoveryClientApplication.getCarName(name);
	}

	@RequestMapping(value = "/restTemplate/{name}")
	public Object getCarWithRestTemplate(@PathVariable String name) {
		return discoveryClientApplication.getCarNameRest(name);
	}

	@RequestMapping(value = "/feign/{name}")
	public Object getCarWithFeign(@PathVariable String name) {
		return discovery.getCarFeign(name);
	}
}
