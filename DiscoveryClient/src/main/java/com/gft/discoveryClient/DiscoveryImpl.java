package com.gft.discoveryClient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DiscoveryImpl {
	@Autowired
	private DiscoveryClient discoveryClient;

	@Autowired
	RestTemplate restTemplate;

	public Object getCarName(String name) {
		RestTemplate restTemplate = new RestTemplate();
		java.util.List<ServiceInstance> instances = discoveryClient.getInstances("carAgencyService");
		if (instances.size() == 0)
			return null;
		String serviceUri = String.format("%s/cars/v0/%s", instances.get(0).getUri().toString(), name);

		ResponseEntity<Object> restExchange = restTemplate.exchange(serviceUri, HttpMethod.GET, null, Object.class,
				name);
		return restExchange.getBody();
	}

	public Object getCarNameRest(String name) {
		ResponseEntity<Object> restExchange = restTemplate.exchange("http://carAgencyService/cars/v0/{name}",
				HttpMethod.GET, null, Object.class, name);
		return restExchange.getBody();
	}
}
