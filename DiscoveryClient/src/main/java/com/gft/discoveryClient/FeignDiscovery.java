package com.gft.discoveryClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "carAgencyService")
public interface FeignDiscovery {
	@RequestMapping(method = RequestMethod.GET, value = "cars/v0/{name}")
	Object getCarFeign(@PathVariable String name);

}
