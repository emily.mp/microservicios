package com.gft.carAgency.dao.service;

import java.util.List;

import com.gft.carAgency.dao.model.Car;
import com.gft.carAgency.facade.dto.DtoCar;

public interface ICarService {

	List<DtoCar> findCars();

	DtoCar findCar(Integer id);

	void deleteCar(Integer id);

	void saveCar(Car car);

	void updateCar(Integer id, Car carUpdate);
}
