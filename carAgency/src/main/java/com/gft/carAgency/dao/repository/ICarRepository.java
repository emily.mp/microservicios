package com.gft.carAgency.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gft.carAgency.dao.model.Car;

@Repository
public interface ICarRepository extends JpaRepository<Car, Integer> {

}
