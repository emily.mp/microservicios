package com.gft.carAgency.dao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gft.carAgency.dao.model.Car;
import com.gft.carAgency.dao.repository.ICarRepository;
import com.gft.carAgency.dao.service.ICarService;
import com.gft.carAgency.facade.controller.CarMapper;
import com.gft.carAgency.facade.dto.DtoCar;

@Service
public class CarService implements ICarService {

	@Autowired
	private ICarRepository repo;
	@Autowired
	CarMapper carMapper;

	@Override
	public List<DtoCar> findCars() {
		return carMapper.toDtoCars(repo.findAll());
		// return new ArrayList<DtoCar>();
	}

	@Override
	public DtoCar findCar(Integer id) {
		return carMapper.toDtoCarOutter(repo.findById(id).get());
		// return new DtoCar(1, "Sport", "Ford");
	}

	@Override
	public void deleteCar(Integer id) {
		repo.deleteById(id);
	}

	@Override
	public void updateCar(Integer id, Car carUpdate) {
		Car car = repo.findById(id).get();
		if (carUpdate.getBrand() != null) {
			car.setBrand(carUpdate.getBrand());
		}
		if (carUpdate.getModel() != null) {
			car.setModel(carUpdate.getModel());
		}
		if (carUpdate.getPrice() != null) {
			car.setPrice(carUpdate.getPrice());
		}
		if (carUpdate.getType() != null) {
			car.setType(carUpdate.getType());
		}
		repo.save(car);
	}

	@Override
	public void saveCar(Car car) {
		repo.save(car);
	}

}
