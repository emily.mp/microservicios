package com.gft.carAgency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages = { "com.gft.carAgency" })
//@EntityScan("com.gft.carAgency.dao.model")
//@EnableJpaRepositories("com.gft.carAgency.dao.repository")
//@RefreshScope
public class CarAgencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarAgencyApplication.class, args);
	}

}
