package com.gft.carAgency.facade.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.gft.carAgency.dao.model.Car;
import com.gft.carAgency.facade.dto.DtoCar;

@Service
public class Mapper implements CarMapper {

	@Override
	public Car toCarInner(DtoCar dtoCar) {
		Car car = new Car();
		car.setIdCar(dtoCar.getIdCar());
		car.setBrand(dtoCar.getBrand());
		car.setModel(dtoCar.getModel());
		car.setPrice(dtoCar.getPrice());
		car.setType(dtoCar.getType());
		return car;
	}

	@Override
	public DtoCar toDtoCarOutter(Car dtoCar) {
		DtoCar car = new DtoCar();
		car.setIdCar(dtoCar.getIdCar());
		car.setBrand(dtoCar.getBrand());
		car.setModel(dtoCar.getModel());
		car.setPrice(dtoCar.getPrice());
		car.setType(dtoCar.getType());
		return car;
	}

	@Override
	public List<DtoCar> toDtoCars(List<Car> cars) {
		List<DtoCar> dtoCars = new ArrayList<DtoCar>();
		for (Car dtoCar : cars) {
			DtoCar car = new DtoCar();
			car.setIdCar(dtoCar.getIdCar());
			car.setBrand(dtoCar.getBrand());
			car.setModel(dtoCar.getModel());
			car.setPrice(dtoCar.getPrice());
			car.setType(dtoCar.getType());
			dtoCars.add(car);
		}
		return dtoCars;
	}

}
