package com.gft.carAgency.facade.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gft.carAgency.dao.service.ICarService;
import com.gft.carAgency.facade.dto.DtoCar;

@RestController
@RequestMapping("cars/v0")
public class CarController {
	@Autowired
	ICarService service;
	@Autowired
	CarMapper carMapper;

	@RequestMapping(method = RequestMethod.POST, produces = { "application/json" })
	public void createCar(@RequestBody DtoCar car) {
		service.saveCar(carMapper.toCarInner(car));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PATCH, produces = { "application/json" })
	public void updateCar(@PathVariable(name = "id") String id, @RequestBody DtoCar car) {
		service.updateCar(Integer.parseInt(id), carMapper.toCarInner(car));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public DtoCar getCar(@PathVariable(name = "id") String id) {
		return service.findCar(Integer.parseInt(id));
	}

	@RequestMapping(method = RequestMethod.GET, produces = { "application/json" })
	public List<DtoCar> getCars() {
		return service.findCars();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = { "application/json" })
	public void deleteCar(@PathVariable(name = "id") String id) {
		service.deleteCar(Integer.parseInt(id));
	}

}
