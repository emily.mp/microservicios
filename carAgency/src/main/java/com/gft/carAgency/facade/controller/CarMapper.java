package com.gft.carAgency.facade.controller;

import java.util.List;

import org.mapstruct.Mapper;

import com.gft.carAgency.dao.model.Car;
import com.gft.carAgency.facade.dto.DtoCar;

@Mapper
public interface CarMapper {

	Car toCarInner(DtoCar dtoCar);

	DtoCar toDtoCarOutter(Car car);

	List<DtoCar> toDtoCars(List<Car> cars);
}
